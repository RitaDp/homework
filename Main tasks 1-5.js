/**
* 1. Напишите код, который будет присваивать переменной "result" значение суммы переменных "x" и "y"
* - в случае если x < y, разность "x" и "y" - в случае если x > y, и их произведение в остальных случаях.
*/

let result
let x = 6
let y = 6
if (x < y){
     result = x+y
} else if (x > y){
     result = x-y
} else {
     result = x*y
}

//console.log(result)



/**
* 2. Есть два массива случайной длины заполненные случайными числами.
* Вам нужно написать функцию, которая считает сумму всех элементов обоих массивов.
*/

let randNumb1 = [3, 5, 3, 5, 9]
let randNumb2 = [2, 8, 6, 3, -6, 5, 1]

function arraySum (rn1, rn2) {
    let arr1 = 0
    let arr2 = 0
    for (let i = 0; i < rn1.length; i++) {
        arr1 += rn1[i]
    }
    for (let i = 0; i < rn2.length; i++) {
        arr2 += rn2[i]
    }
    return arr1 + arr2
}

let allSum = arraySum(randNumb1, randNumb2)
//console.log(allSum)


/**
* 3. Дан массив, в котором содержатся элементы true и false. Написать функцию, которая подсчитает количество элементов true в массиве.
*/

let arrBoolean = [true, false, false, true, true, true]

function countTrue (arrB) {
    let count = 0
     for (let t of arrB){
          if (t === true)
              count++
    }
    return count
}

let numbTrue = countTrue(arrBoolean)
//console.log(numbTrue)

/**
* 4. Напишите функцию, которая вычисляет двойной факториал числа
*/

let n = 6

function countFactorial (f){
    if (f === 0 || f === 1)
        return 1
    if (f > 0)
        return countFactorial(f-2) * f
}

let doubleFactorial = countFactorial(n)
//console.log(doubleFactorial)

/**
* 5. Дано два объекта person, в них содержатся поля name и age.
* 5.1 Написать функцию compareAge, которая будет сравнивать два объекта по возрасту и возвращать шаблонную строку как результат.
Шаблонная строка выглядит следующим образом: <Person 1 name> is <older | younger | the same age as> <Person 2 name>
*/

const person1 = {
    name: "Kate",
    age: 28
}

const person2 = {
    name: "Peter",
    age: 31
}

function compareAge (p1, p2) {
    if (p1.age < p2.age) {
        return p1.name + " is younger " + p2.name
    } else if (p1.age > p2.age){
        return p1.name + " is older " + p2.name
    } else if (p1.age === p2.age){
        return p1.name + " is the same age as " + p2.name
    }
}

let compare = compareAge(person1,person2)
//console.log(compare)

/**
* 5.2 Создать массив из нескольких объектов person (3-5 элементов) и отсортировать его по возрасту:
*/

const person = [
    {name: "Ivan", age: 28},
    {name: "Darina", age: 22},
    {name: "Maria", age: 31},
    {name: "Victor", age: 15},
    {name: "Tanya", age: 45}
]

/**
* а) по возрастанию
*/


person.sort(sortByAgeUp)

function sortByAgeUp (a, b){
    if (a.age > b.age)
        return 1
    if (a.age < b.age)
        return -1
    return 0
}

//console.log(person)

/**
* б) по спаданию
*/

person.sort(sortByAgeDown)

function sortByAgeDown (a, b){
    if (a.age > b.age)
        return -1
    if (a.age < b.age)
        return 1
    return 0
}

//console.log(person)
